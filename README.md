# ideapad-control-panel

Finally take control over your Ideapad functionalities and power management without moving from Linux !

## Introduction

Lenovo Ideapad Control Panel (LICP) is a model-specific latop control panel.
It allows you to enable / disable functions such as battery conservation mode or USB charging and change power mode.
All that without rebooting and directly from your Linux-based system.
LICP also provide a way of monitoring your battery health.

![Main Menu](./doc/main_menu.png)
![Batterty Info Menu](./doc/battery_info.png)

## Compatibility

This application is not compatible with all model of laptop.
It has been designed for usage with Lenovo Ideapad functionalities in mind but has yet been tested only with the following models :
* Ideapad 5 15ALC05

## Installation

### Dependencies
```
$ sudo apt install openjdk-17-jdk acpi-call-dkms pkexec
```
Note : The given package ```acpi-call-dkms``` is recommended for Debian-type distribution but can be specific to your OS.

### Compile & install
```
$ git clone https://....
$ cd lenovo-ideapad-control-panel
$ sudo ./install.sh
```
The ```install.sh``` installation script will, by default, install the control panel in the ```/opt/licp``` directory. You can change this behavior by editing the ```install_dir``` variable in the header of the script.

### Usage
```
$ licp
```
Note : You will need root access to use this app since it need to talk with the ACPI system.

## Disclaimer

Reference to any Lenovo products, services, processes, or other information and/or use of Lenovo Trademarks does not constitute or imply endorsement, sponsorship, or recommendation thereof by Lenovo.

The use of Lenovo®, Lenovo Legion™, Yoga®, IdeaPad® or other trademarks within this website and associated tools and libraries is only to provide a recognisable identifier to users to enable them to associate that these tools will work with Lenovo laptops.