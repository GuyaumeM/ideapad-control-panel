#!/bin/bash

install_dir="/opt/licp/"
user=$(whoami)

if [ "$user" != "root" ]
then
        echo "Please run as root."
else
	cd "$(dirname "$0")"
	mkdir -p ./bin
	javac -d ./bin ./src/controller/*.java ./src/model/*.java ./src/view/*.java ./src/Main.java
	mkdir -p ${install_dir}
	cp -r ./assets ${install_dir}
	cp -r ./bin ${install_dir}
	cp ./run.sh ${install_dir}
	echo "acpi_call" >> /etc/modules-load.d/modules.conf
	modprobe acpi_call
	ln -s ${install_dir}run.sh /usr/bin/licp
fi

exit
