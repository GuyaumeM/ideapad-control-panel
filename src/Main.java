import controller.MainController;

/**
 * Main class
 * Starts the application
 * 
 * @author Guyaume MORICE
 */
public class Main {

    /**
     * Enter point of execution
     * 
     * @param args arguments
     */
    public static void main(String[] args) {
        new MainController();
    }
}