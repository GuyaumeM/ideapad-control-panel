package controller;

import java.awt.event.*;
import model.*;
import view.*;

/**
 * BatteryInfoController class
 * Controller for the battery informations window
 * 
 * @author Guyaume MORICE
 */
public class BatteryInfoController implements ActionListener, WindowListener {

    // Controller logic links
    private MainController mainController;
    private BatteryCall batteryCall;
    private BatteryInfoGUI gui;

    // BatteryInfoGUI links
    private String batteryManufacturer;
    private String batteryModel;
    private String batterySerialNumber;
    private String batteryChemistry;
    private float batteryDesignVoltage;
    private float batteryDesignCapacity;
    private float currentBatteryCapacity;
    private float currentBatteryStateOfCharge;
    private float currentBatteryPercentage;
    private float currentBatteryWear;
    private float currentBatteryPower;
    private float currentBatteryVoltage;
    private float currentBatteryCurrent;
    private String currentBatteryStatus;
    private Thread updateThread;

    /**
     * Constructor of the BatteryInfoController class
     * 
     * @param batteryCall system call controller for battery
     * @throws DriverException if an error occured with the driver
     */
    public BatteryInfoController(BatteryCall batteryCall, MainController mainController) throws DriverException {
        this.mainController = mainController;
        this.batteryCall = batteryCall;
        this.gui = new BatteryInfoGUI(this);
        this.batteryManufacturer = this.batteryCall.getManufacturer();
        this.batteryModel = this.batteryCall.getModelName();
        this.batterySerialNumber = this.batteryCall.getSerialNumber();
        this.batteryChemistry = this.batteryCall.getTechnology();
        this.batteryDesignVoltage = this.batteryCall.getVoltageMinDesign();
        this.batteryDesignCapacity = this.batteryCall.getEnergyFullDesign();
        this.gui.getBatteryManufacturerLabel().setText(this.batteryManufacturer);
        this.gui.getBatteryModelLabel().setText(this.batteryModel);
        this.gui.getBatterySerialNumberLabel().setText(this.batterySerialNumber);
        this.gui.getBatteryChemistryLabel().setText(this.batteryChemistry);
        this.gui.getBatteryDesignVoltageLabel().setText(String.format("%.2f", this.batteryDesignVoltage) + " V");
        this.gui.getBatteryDesignCapacityLabel().setText(String.format("%.2f", this.batteryDesignCapacity) + " Wh");

        // Update the values and the display every second using a background thread
        this.updateThread = new Thread() {
            public void run() {
                while (true) {
                    try {
                        updateValues();
                        updateDisplay();
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        break;
                    } catch (DriverException e) {
                        new ErrorGUI(e);
                        System.exit(1);
                    }
                }
            }
        };
        this.updateThread.start();
    }

    /**
     * Update the values to display
     * 
     * @throws DriverException if an error occured with the driver
     */
    public void updateValues() throws DriverException {
        this.currentBatteryCapacity = this.batteryCall.getEnergyFull();
        this.currentBatteryStateOfCharge = this.batteryCall.getEnergyNow();
        this.currentBatteryPercentage = this.batteryCall.getPercentage();
        this.currentBatteryWear = this.batteryCall.getWearLevel();
        this.currentBatteryPower = this.batteryCall.getPowerNow();
        this.currentBatteryVoltage = this.batteryCall.getVoltageNow();
        this.currentBatteryCurrent = this.batteryCall.getCurrent();
        this.currentBatteryStatus = this.batteryCall.getStatus();
        if (!this.batteryCall.isConnected()) {
            this.currentBatteryPower = -this.currentBatteryPower;
        }
    }

    /**
     * Update the GUI elements with the current values
     */
    public void updateDisplay() {
        this.gui.getBatteryCapacityLabel().setText(String.format("%.2f", this.currentBatteryCapacity) + " Wh");
        this.gui.getBatteryStateOfChargeLabel()
                .setText(String.format("%.2f", this.currentBatteryStateOfCharge) + " Wh");
        this.gui.getBatteryPercentageLabel().setText(String.format("%.2f", this.currentBatteryPercentage) + "%");
        this.gui.getBatteryWearLabel().setText(String.format("%.2f", this.currentBatteryWear) + "%");
        this.gui.getBatteryPowerLabel().setText(String.format("%.2f", this.currentBatteryPower) + " W");
        this.gui.getBatteryVoltageLabel().setText(String.format("%.2f", this.currentBatteryVoltage) + " V");
        this.gui.getBatteryCurrentLabel().setText(String.format("%.2f", this.currentBatteryCurrent) + " A");
        this.gui.getBatteryStatusLabel().setText(this.currentBatteryStatus);
    }

    /**
     * Close the window and stop the update thread
     */
    public void close() {
        this.gui.dispose();
        this.updateThread.interrupt();
        this.mainController.batteryInfoClosed();
    }

    /**
     * Action performed method for the controller to handle the button presses
     * 
     * @param e event to handle
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.gui.getCloseButton()) {
            this.close();
        }
    }

    /**
     * Window action performed method for the controller to handle the window
     * closing
     * 
     * @param e event to handle
     */
    public void windowClosed(WindowEvent e) {
        this.close();
    }

    /**
     * Unused method
     * 
     * @param e event to handle
     */
    public void windowOpened(WindowEvent e) {
    }

    /**
     * Unused method
     * 
     * @param e event to handle
     */
    public void windowClosing(WindowEvent e) {
    }

    /**
     * Unused method
     * 
     * @param e event to handle
     */
    public void windowIconified(WindowEvent e) {
    }

    /**
     * Unused method
     * 
     * @param e event to handle
     */
    public void windowDeiconified(WindowEvent e) {
    }

    /**
     * Unused method
     * 
     * @param e event to handle
     */
    public void windowActivated(WindowEvent e) {
    }

    /**
     * Unused method
     * 
     * @param e event to handle
     */
    public void windowDeactivated(WindowEvent e) {
    }
}
