package controller;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import model.*;
import view.*;

/**
 * MainController class
 * Application controller for the main menu
 * 
 * @author Guyaume MORICE
 */
public class MainController implements ActionListener {

    // Controller logic links
    private BatteryCall batteryCall;
    private AcpiCall acpiCall;
    private BatteryInfoController batteryInfoController;
    private Thread updateThread;

    // MainGUI links
    private MainGUI gui;
    private int currentPowerMode;
    private boolean currentFan;
    private boolean currentFn;
    private boolean currentUsb;
    private boolean currentTouchpad;
    private boolean currentBatteryConservation;
    private boolean isCharging;
    private boolean isConnected;
    private float currentBatteryPercentage;
    private float currentBatteryWear;
    private float currentBatteryPower;
    private float currentBatteryVoltage;

    // Constants
    private final Color DISABLED_COLOR = Color.WHITE;
    private final Color ENABLED_COLOR = new Color(69, 121, 240);
    private final String ASSETS_PATH = System.getProperty("java.class.path") + "/../assets/";

    /**
     * Contructor for the MainController class
     */
    public MainController() {
        try {
            this.batteryCall = new BatteryCall();
            this.acpiCall = new AcpiCall();
            this.gui = new MainGUI(this);
            this.currentPowerMode = this.acpiCall.getPowerMode();
            this.currentFan = this.acpiCall.getFan();
            this.currentFn = !this.acpiCall.getFn();
            this.currentUsb = this.acpiCall.getUsb();
            this.currentTouchpad = !this.acpiCall.getTouchpad();
            this.currentBatteryConservation = this.acpiCall.getBatteryConservation();
            this.isCharging = false;

            // Update the values and the display every second using a background thread
            this.updateThread = new Thread() {
                public void run() {
                    while (true) {
                        try {
                            updateValues();
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            break;
                        } catch (DriverException e) {
                            new ErrorGUI(e);
                        }
                    }
                }
            };
            this.updateThread.start();
            this.initDisplay();
        } catch (NotCompatibleException e) {
            new ErrorGUI(e);
        } catch (DriverException e) {
            new ErrorGUI(e);
        }

    }

    /**
     * Initialize the elements of the GUI
     */
    private void initDisplay() {
        this.gui.getPower1Button().setBackground(DISABLED_COLOR);
        this.gui.getPower2Button().setBackground(DISABLED_COLOR);
        this.gui.getPower3Button().setBackground(DISABLED_COLOR);
        this.gui.getPowerFanButton().setBackground(DISABLED_COLOR);
        this.gui.getUtilsFnButton().setBackground(DISABLED_COLOR);
        this.gui.getUtilsUsbButton().setBackground(DISABLED_COLOR);
        this.gui.getUtilsTouchpadButton().setBackground(DISABLED_COLOR);
        this.gui.getBatteryConservationButton().setBackground(DISABLED_COLOR);
        this.gui.getBatteryPercentageButton().setBackground(DISABLED_COLOR);
        this.gui.getBatteryInfoButton().setBackground(DISABLED_COLOR);
        this.gui.getBatteryConservationButton().setEnabled(true);
        this.gui.getBatteryPercentageButton().setEnabled(true);
        this.gui.getPower1Button().setIcon(new ImageIcon(ASSETS_PATH + "pw1d.png"));
        this.gui.getPower2Button().setIcon(new ImageIcon(ASSETS_PATH + "pw2d.png"));
        this.gui.getPower3Button().setIcon(new ImageIcon(ASSETS_PATH + "pw3d.png"));
        this.gui.getPowerFanButton().setIcon(new ImageIcon(ASSETS_PATH + "ftd.png"));
        this.gui.getUtilsFnButton().setIcon(new ImageIcon(ASSETS_PATH + "fxd.png"));
        this.gui.getUtilsUsbButton().setIcon(new ImageIcon(ASSETS_PATH + "ucd.png"));
        this.gui.getUtilsTouchpadButton().setIcon(new ImageIcon(ASSETS_PATH + "dtd.png"));
        this.gui.getBatteryConservationButton().setIcon(new ImageIcon(ASSETS_PATH + "bcd.png"));
        switch (this.currentPowerMode) {
            case 1:
                this.gui.getPower1Button().setBackground(ENABLED_COLOR);
                this.gui.getPower1Button().setIcon(new ImageIcon(ASSETS_PATH + "pw1e.png"));
                break;
            case 2:
                this.gui.getPower2Button().setBackground(ENABLED_COLOR);
                this.gui.getPower2Button().setIcon(new ImageIcon(ASSETS_PATH + "pw2e.png"));
                break;
            case 3:
                this.gui.getPower3Button().setBackground(ENABLED_COLOR);
                this.gui.getPower3Button().setIcon(new ImageIcon(ASSETS_PATH + "pw3e.png"));
                break;
        }
        if (this.currentFan) {
            this.gui.getPowerFanButton().setBackground(ENABLED_COLOR);
            this.gui.getPowerFanButton().setIcon(new ImageIcon(ASSETS_PATH + "fte.png"));
        }
        if (this.currentFn) {
            this.gui.getUtilsFnButton().setBackground(ENABLED_COLOR);
            this.gui.getUtilsFnButton().setIcon(new ImageIcon(ASSETS_PATH + "fxe.png"));
        }
        if (this.currentUsb) {
            this.gui.getUtilsUsbButton().setBackground(ENABLED_COLOR);
            this.gui.getUtilsUsbButton().setIcon(new ImageIcon(ASSETS_PATH + "uce.png"));
        }
        if (this.currentTouchpad) {
            this.gui.getUtilsTouchpadButton().setBackground(ENABLED_COLOR);
            this.gui.getUtilsTouchpadButton().setIcon(new ImageIcon(ASSETS_PATH + "dte.png"));
        }
        if (this.currentBatteryConservation) {
            this.gui.getBatteryConservationButton().setBackground(ENABLED_COLOR);
            this.gui.getBatteryConservationButton().setIcon(new ImageIcon(ASSETS_PATH + "bce.png"));
        }
        if (!this.isConnected) {
            this.gui.getBatteryPercentageButton().setEnabled(false);
        }
    }

    /**
     * Charge the battery up to the given percentage using a background thread
     * 
     * @param charge percentage to charge up to (between 60 and 100)
     * @throws DriverException if an error occured with the driver
     */
    private void chargeUpTo(int charge) throws DriverException {
        Thread chargeThread = new Thread() {
            public void run() {
                try {
                    updateCharging(true);

                    while (isCharging) {
                        try {
                            if ((currentBatteryPercentage >= charge) || (!isConnected)) {
                                updateCharging(false);
                            }
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            isCharging = false;
                        }
                    }

                    updateCharging(false);
                } catch (DriverException e) {
                    new ErrorGUI(e);
                }
            }
        };
        chargeThread.start();
    }

    /**
     * Update the values and the GUI
     * 
     * @throws DriverException if an error occured with the driver
     */
    private void updateValues() throws DriverException {
        // Update the current values
        this.isConnected = this.batteryCall.isConnected();
        this.currentBatteryPercentage = this.batteryCall.getPercentage();
        this.currentBatteryWear = this.batteryCall.getWearLevel();
        this.currentBatteryPower = this.batteryCall.getPowerNow();
        if (!this.isConnected) {
            this.currentBatteryPower = -this.currentBatteryPower;
        }
        this.currentBatteryVoltage = this.batteryCall.getVoltageNow();

        // Adapt the display to the current values
        if (this.isConnected) {
            this.gui.getBatteryPercentageButton().setEnabled(true);
        } else {
            this.gui.getBatteryPercentageButton().setEnabled(false);
            this.updateCharging(false);
        }
        this.gui.getBatteryPercentageLabel().setText(String.format("%.2f", this.currentBatteryPercentage) + "%");
        this.gui.getBatteryWearLabel().setText(String.format("%.2f", this.currentBatteryWear) + "%");
        this.gui.getBatteryPowerLabel().setText(String.format("%.2f", this.currentBatteryPower) + " W");
        this.gui.getBatteryVoltageLabel().setText(String.format("%.2f", this.currentBatteryVoltage) + " V");
    }

    /**
     * Action to perform when "Battery Saving" button is pressed
     * 
     * @throws DriverException if an error occured with the driver
     */
    public void powerMode1Pressed() throws DriverException {
        this.currentPowerMode = 1;
        if (this.acpiCall.getPowerMode() != 1) {
            this.acpiCall.setPowerMode(1);
        }
        this.gui.getPower1Button().setBackground(ENABLED_COLOR);
        this.gui.getPower2Button().setBackground(DISABLED_COLOR);
        this.gui.getPower3Button().setBackground(DISABLED_COLOR);
        this.gui.getPower1Button().setIcon(new ImageIcon(ASSETS_PATH + "pw1e.png"));
        this.gui.getPower2Button().setIcon(new ImageIcon(ASSETS_PATH + "pw2d.png"));
        this.gui.getPower3Button().setIcon(new ImageIcon(ASSETS_PATH + "pw3d.png"));
    }

    /**
     * Action to perform when "Intelligent Cooling" button is pressed
     * 
     * @throws DriverException if an error occured with the driver
     */
    public void powerMode2Pressed() throws DriverException {
        this.currentPowerMode = 2;
        if (this.acpiCall.getPowerMode() != 2) {
            this.acpiCall.setPowerMode(2);
        }
        this.gui.getPower1Button().setBackground(DISABLED_COLOR);
        this.gui.getPower2Button().setBackground(ENABLED_COLOR);
        this.gui.getPower3Button().setBackground(DISABLED_COLOR);
        this.gui.getPower1Button().setIcon(new ImageIcon(ASSETS_PATH + "pw1d.png"));
        this.gui.getPower2Button().setIcon(new ImageIcon(ASSETS_PATH + "pw2e.png"));
        this.gui.getPower3Button().setIcon(new ImageIcon(ASSETS_PATH + "pw3d.png"));
    }

    /**
     * Action to perform when "Extreme Performance" button is pressed
     * 
     * @throws DriverException if an error occured with the driver
     */
    public void powerMode3Pressed() throws DriverException {
        this.currentPowerMode = 3;
        if (this.acpiCall.getPowerMode() != 3) {
            this.acpiCall.setPowerMode(3);
        }
        this.gui.getPower1Button().setBackground(DISABLED_COLOR);
        this.gui.getPower2Button().setBackground(DISABLED_COLOR);
        this.gui.getPower3Button().setBackground(ENABLED_COLOR);
        this.gui.getPower1Button().setIcon(new ImageIcon(ASSETS_PATH + "pw1d.png"));
        this.gui.getPower2Button().setIcon(new ImageIcon(ASSETS_PATH + "pw2d.png"));
        this.gui.getPower3Button().setIcon(new ImageIcon(ASSETS_PATH + "pw3e.png"));
    }

    /**
     * Action to perform when "Fan Turbo" button is pressed
     * 
     * @throws DriverException
     */
    public void powerFanPressed() throws DriverException {
        this.currentFan = !this.currentFan;
        if (this.acpiCall.getFan() != this.currentFan) {
            this.acpiCall.setFan(this.currentFan);
        }
        this.gui.getPowerFanButton().setBackground(this.currentFan ? ENABLED_COLOR : DISABLED_COLOR);
        if (this.currentFan) {
            this.gui.getPowerFanButton().setIcon(new ImageIcon(ASSETS_PATH + "fte.png"));
        } else {
            this.gui.getPowerFanButton().setIcon(new ImageIcon(ASSETS_PATH + "ftd.png"));
        }
    }

    /**
     * Action to perform when "Function Keys" button is pressed
     * 
     * @throws DriverException if an error occured with the driver
     */
    public void utilsFnPressed() throws DriverException {
        this.currentFn = !this.currentFn;
        if (this.acpiCall.getFn() == this.currentFn) {
            this.acpiCall.setFn(!this.currentFn);
        }
        this.gui.getUtilsFnButton().setBackground(this.currentFn ? ENABLED_COLOR : DISABLED_COLOR);
        this.gui.getUtilsFnButton()
                .setIcon(new ImageIcon(this.currentFn ? ASSETS_PATH + "fxe.png" : ASSETS_PATH + "fxd.png"));
    }

    /**
     * Action to perform when "USB Charging" button is pressed
     * 
     * @throws DriverException if an error occured with the driver
     */
    public void utilsUsbPressed() throws DriverException {
        this.currentUsb = !this.currentUsb;
        if (this.acpiCall.getUsb() != this.currentUsb) {
            this.acpiCall.setUsb(this.currentUsb);
        }
        this.gui.getUtilsUsbButton().setBackground(this.currentUsb ? ENABLED_COLOR : DISABLED_COLOR);
        this.gui.getUtilsUsbButton()
                .setIcon(new ImageIcon(this.currentUsb ? ASSETS_PATH + "uce.png" : ASSETS_PATH + "ucd.png"));
    }

    /**
     * Action to perform when "Touchpad Lock" button is pressed
     * 
     * @throws DriverException if an error occured with the driver
     */
    public void utilsTouchpadPressed() throws DriverException {
        this.currentTouchpad = !this.currentTouchpad;
        if (this.acpiCall.getTouchpad() == this.currentTouchpad) {
            this.acpiCall.setTouchpad(!this.currentTouchpad);
        }
        this.gui.getUtilsTouchpadButton().setBackground(this.currentTouchpad ? ENABLED_COLOR : DISABLED_COLOR);
        this.gui.getUtilsTouchpadButton()
                .setIcon(new ImageIcon(this.currentTouchpad ? ASSETS_PATH + "dte.png" : ASSETS_PATH + "dtd.png"));
    }

    /**
     * Action to perform when "Battery Conservation" button is pressed
     * 
     * @throws DriverException if an error occured with the driver
     */
    public void batteryConservationPressed() throws DriverException {
        this.currentBatteryConservation = !this.currentBatteryConservation;
        if (this.acpiCall.getBatteryConservation() != this.currentBatteryConservation) {
            this.acpiCall.setBatteryConservation(this.currentBatteryConservation);
        }
        this.gui.getBatteryConservationButton()
                .setBackground(this.currentBatteryConservation ? ENABLED_COLOR : DISABLED_COLOR);
        this.gui.getBatteryConservationButton().setIcon(
                new ImageIcon(this.currentBatteryConservation ? ASSETS_PATH + "bce.png" : ASSETS_PATH + "bcd.png"));
    }

    /**
     * Action to perform when the button to set the battery percentage is pressed
     * 
     * @throws DriverException if an error occured with the driver
     */
    public void batteryPercentagePressed() throws DriverException {
        if (this.isCharging) {
            updateCharging(false);
        } else {
            try {
                int charge = Integer.parseInt(this.gui.getBatteryPercentageField().getText());
                if ((charge >= 60) && (charge <= 100) && this.isConnected) {
                    this.chargeUpTo(charge);
                }
            } catch (NumberFormatException e) {
            }
        }
    }

    /**
     * Update the charging status, and the GUI.
     * When charging, conservation mode must be disabled and as such parts of the
     * GUI must be updated.
     * 
     * @param charging new charging status
     * @throws DriverException if an error occured with the driver
     */
    private void updateCharging(boolean charging) throws DriverException {
        if (this.isCharging != charging) {
            isCharging = charging;
            gui.getBatteryPercentageButton().setBackground(charging ? ENABLED_COLOR : DISABLED_COLOR);
            if (acpiCall.getBatteryConservation() == charging) {
                this.currentBatteryConservation = !charging;
                acpiCall.setBatteryConservation(!charging);
                gui.getBatteryConservationButton().setBackground(!charging ? ENABLED_COLOR : DISABLED_COLOR);
                gui.getBatteryConservationButton()
                        .setIcon(new ImageIcon(!charging ? ASSETS_PATH + "bce.png" : ASSETS_PATH + "bcd.png"));
                gui.getBatteryConservationButton().setEnabled(!charging);
            }
        }
    }

    /**
     * Notify the main controller that the battery info window is closed
     * 
     * @throws DriverException
     */
    public void batteryInfoClosed() {
        this.batteryInfoController = null;
    }

    /**
     * Action to perform when "Battery Info" button is pressed
     * 
     * @throws DriverException if an error occured with the driver
     */
    public void batteryInfoPressed() throws DriverException {
        if (this.batteryInfoController != null) {
            this.batteryInfoController.close();
        } else {
            this.batteryInfoController = new BatteryInfoController(this.batteryCall, this);
        }
    }

    /**
     * Action performed method for the controller to handle the button presses
     * 
     * @param e event to handle
     */
    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getSource() == this.gui.getPower1Button()) {
                this.powerMode1Pressed();
            } else if (e.getSource() == this.gui.getPower2Button()) {
                this.powerMode2Pressed();
            } else if (e.getSource() == this.gui.getPower3Button()) {
                this.powerMode3Pressed();
            } else if (e.getSource() == this.gui.getPowerFanButton()) {
                this.powerFanPressed();
            } else if (e.getSource() == this.gui.getUtilsFnButton()) {
                this.utilsFnPressed();
            } else if (e.getSource() == this.gui.getUtilsUsbButton()) {
                this.utilsUsbPressed();
            } else if (e.getSource() == this.gui.getUtilsTouchpadButton()) {
                this.utilsTouchpadPressed();
            } else if (e.getSource() == this.gui.getBatteryConservationButton()) {
                this.batteryConservationPressed();
            } else if (e.getSource() == this.gui.getBatteryPercentageButton()) {
                this.batteryPercentagePressed();
            } else if (e.getSource() == this.gui.getBatteryInfoButton()) {
                this.batteryInfoPressed();
            }
        } catch (DriverException ex) {
            new ErrorGUI(ex);
        }
    }

}
