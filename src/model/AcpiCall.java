package model;

import java.io.*;

/**
 * AcpiCall class
 * Model to access acpi_call and ideapad_acpi drivers
 * 
 * @author Guyaume MORICE
 */
public class AcpiCall {

    // Constants
    private final String ACPI_PATH = "/proc/acpi/call";
    private final String IDEAPAD_ACPI_PATH = "/sys/bus/platform/drivers/ideapad_acpi/VPC2004:00/";

    /**
     * Constructor of the AcpiCall class
     * 
     * @throws NotCompatibleException if the computer is not compatible with this
     *                                program
     */
    public AcpiCall() throws NotCompatibleException {
        if (!this.verifyCompatibility()) {
            throw new NotCompatibleException("Your computer is not compatible with this program");
        }
    }

    /**
     * Verify if the computer is compatible with this program
     * 
     * @return true if the computer is compatible, false otherwise
     */
    private boolean verifyCompatibility() {
        File file = new File(ACPI_PATH);
        File file2 = new File(IDEAPAD_ACPI_PATH);
        return (file.exists() && file2.exists());
    }

    /**
     * Get the currently used power mode
     * 
     * @return the currently used power mode
     * @throws DriverException if an error occured with the driver
     */
    public int getPowerMode() throws DriverException {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(ACPI_PATH));
            writer.write("\\_SB.PCI0.LPC0.EC0.FCMO");
            writer.close();
            BufferedReader reader = new BufferedReader(new FileReader(ACPI_PATH));
            String line = reader.readLine();
            reader.close();
            char lastChar = line.charAt(line.length() - 2);
            int ret = 0;
            if (lastChar == '0') {
                ret = 2;
            } else if (lastChar == '1') {
                ret = 3;
            } else if (lastChar == '2') {
                ret = 1;
            } else {
                throw new DriverException("Driver error");
            }
            return ret;
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Set the power mode
     * 
     * @param mode the power mode to set (1, 2 or 3)
     * @throws DriverException if an error occured with the driver
     */
    public void setPowerMode(int mode) throws DriverException {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(ACPI_PATH));
            String modeHex = "";
            if (mode == 1) {
                modeHex = "0x0013B001";
            } else if (mode == 2) {
                modeHex = "0x000FB001";
            } else if (mode == 3) {
                modeHex = "0x0012B001";
            }
            writer.write("\\_SB.PCI0.LPC0.EC0.VPC0.DYTC " + modeHex);
            writer.close();
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current fan state
     * 
     * @return true if the fan is on turbo mode, false otherwise
     * @throws DriverException if an error occured with the driver
     */
    public boolean getFan() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(IDEAPAD_ACPI_PATH + "fan_mode"));
            String line = reader.readLine();
            reader.close();
            return !line.equals("133");
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Set the fan state
     * 
     * @param fan true to set the fan on turbo mode, false otherwise
     * @throws DriverException if an error occured with the driver
     */
    public void setFan(boolean fan) throws DriverException {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(IDEAPAD_ACPI_PATH + "fan_mode"));
            if (!fan) {
                writer.write("0");
            } else {
                writer.write("1");
            }
            writer.close();
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current function key state
     * 
     * @return true if Fn is locked, false otherwise
     * @throws DriverException if an error occured with the driver
     */
    public boolean getFn() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(IDEAPAD_ACPI_PATH + "fn_lock"));
            String line = reader.readLine();
            reader.close();
            return line.equals("1");
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Set the function key state
     * 
     * @param fn true to lock Fn, false otherwise
     * @throws DriverException if an error occured with the driver
     */
    public void setFn(boolean fn) throws DriverException {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(IDEAPAD_ACPI_PATH + "fn_lock"));
            if (fn) {
                writer.write("1");
            } else {
                writer.write("0");
            }
            writer.close();
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current USB charging state
     * 
     * @return true if USB charging is enabled, false otherwise
     * @throws DriverException if an error occured with the driver
     */
    public boolean getUsb() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(IDEAPAD_ACPI_PATH + "usb_charging"));
            String line = reader.readLine();
            reader.close();
            return line.equals("1");
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Set the USB charging state
     * 
     * @param usb true to enable USB charging, false otherwise
     * @throws DriverException if an error occured with the driver
     */
    public void setUsb(boolean usb) throws DriverException {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(IDEAPAD_ACPI_PATH + "usb_charging"));
            if (usb) {
                writer.write("1");
            } else {
                writer.write("0");
            }
            writer.close();
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current touchpad state
     * 
     * @return true if the touchpad is enabled, false otherwise
     * @throws DriverException if an error occured with the driver
     */
    public boolean getTouchpad() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(IDEAPAD_ACPI_PATH + "touchpad"));
            String line = reader.readLine();
            reader.close();
            return line.equals("1");
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Set the touchpad state
     * 
     * @param touchpad true to enable the touchpad, false otherwise
     * @throws DriverException if an error occured with the driver
     */
    public void setTouchpad(boolean touchpad) throws DriverException {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(IDEAPAD_ACPI_PATH + "touchpad"));
            if (touchpad) {
                writer.write("1");
            } else {
                writer.write("0");
            }
            writer.close();
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current conservation mode state
     * 
     * @return true if the conservation mode is enabled, false otherwise
     * @throws DriverException if an error occured with the driver
     */
    public boolean getBatteryConservation() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(IDEAPAD_ACPI_PATH + "conservation_mode"));
            String line = reader.readLine();
            reader.close();
            return line.equals("1");
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Set the conservation mode state
     * 
     * @param batteryConservation true to enable the conservation mode, false
     *                            otherwise
     * @throws DriverException if an error occured with the driver
     */
    public void setBatteryConservation(boolean batteryConservation) throws DriverException {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(IDEAPAD_ACPI_PATH + "conservation_mode"));
            if (batteryConservation) {
                writer.write("1");
            } else {
                writer.write("0");
            }
            writer.close();
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

}