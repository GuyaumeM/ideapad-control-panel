package model;

import java.io.*;

/**
 * BatteryCall class
 * Model to access battery informations from the system (usually from the device
 * "BAT0")
 * 
 * @author Guyaume MORICE
 */
public class BatteryCall {

    // Constants
    private static final String BATTERY_PATH = "/sys/class/power_supply/BAT0/";

    /**
     * Constructor of the BatteryCall class
     * 
     * @throws NotCompatibleException if the computer is not compatible with this
     *                                program
     */
    public BatteryCall() throws NotCompatibleException {
        if (!this.verifyCompatibility()) {
            throw new NotCompatibleException("Your computer is not compatible with this program");
        }
    }

    /**
     * Verify if the computer is compatible with this program
     * 
     * @return true if the computer is compatible, false otherwise
     */
    public boolean verifyCompatibility() {
        File file = new File(BATTERY_PATH);
        return file.exists();
    }

    /**
     * Get the current value for "capacity"
     * 
     * @return the current value for "capacity"
     * @throws DriverException if an error occured with the driver
     */
    public int getCapacity() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(BATTERY_PATH + "capacity"));
            String line = reader.readLine();
            reader.close();
            return Integer.parseInt(line);
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current value for "capacity_level"
     * 
     * @return the current value for "capacity_level"
     * @throws DriverException if an error occured with the driver
     */
    public String getCapacityLevel() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(BATTERY_PATH + "capacity_level"));
            String line = reader.readLine();
            reader.close();
            return line;
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current value for "cycle_count"
     * 
     * @return the current value for "cycle_count"
     * @throws DriverException if an error occured with the driver
     */
    public int getCycleCount() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(BATTERY_PATH + "cycle_count"));
            String line = reader.readLine();
            reader.close();
            return Integer.parseInt(line);
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current value for "energy_full"
     * 
     * @return the current value for "energy_full"
     * @throws DriverException if an error occured with the driver
     */
    public float getEnergyFull() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(BATTERY_PATH + "energy_full"));
            String line = reader.readLine();
            reader.close();
            return (float) (Integer.parseInt(line) / 1000000.0);
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current value for "energy_full_design"
     * 
     * @return the current value for "energy_full_design"
     * @throws DriverException if an error occured with the driver
     */
    public float getEnergyFullDesign() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(BATTERY_PATH + "energy_full_design"));
            String line = reader.readLine();
            reader.close();
            return (float) (Integer.parseInt(line) / 1000000.0);
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current value for "energy_now"
     * 
     * @return the current value for "energy_now"
     * @throws DriverException if an error occured with the driver
     */
    public float getEnergyNow() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(BATTERY_PATH + "energy_now"));
            String line = reader.readLine();
            reader.close();
            return (float) (Integer.parseInt(line) / 1000000.0);
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current value for "manufacturer"
     * 
     * @return the current value for "manufacturer"
     * @throws DriverException if an error occured with the driver
     */
    public String getManufacturer() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(BATTERY_PATH + "manufacturer"));
            String line = reader.readLine();
            reader.close();
            return line;
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current value for "model_name"
     * 
     * @return the current value for "model_name"
     * @throws DriverException if an error occured with the driver
     */
    public String getModelName() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(BATTERY_PATH + "model_name"));
            String line = reader.readLine();
            reader.close();
            return line;
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current value for "power_now"
     * 
     * @return the current value for "power_now"
     * @throws DriverException if an error occured with the driver
     */
    public float getPowerNow() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(BATTERY_PATH + "power_now"));
            String line = reader.readLine();
            reader.close();
            return (float) (Integer.parseInt(line) / 1000000.0);
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current value for "present"
     * 
     * @return the current value for "present"
     * @throws DriverException if an error occured with the driver
     */
    public boolean getPresent() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(BATTERY_PATH + "present"));
            String line = reader.readLine();
            reader.close();
            return Integer.parseInt(line) == 1;
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current value for "serial_number"
     * 
     * @return the current value for "serial_number"
     * @throws DriverException if an error occured with the driver
     */
    public String getSerialNumber() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(BATTERY_PATH + "serial_number"));
            String line = reader.readLine();
            reader.close();
            return line;
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current value for "status"
     * 
     * @return the current value for "status"
     * @throws DriverException if an error occured with the driver
     */
    public String getStatus() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(BATTERY_PATH + "status"));
            String line = reader.readLine();
            reader.close();
            return line;
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current value for "technology"
     * 
     * @return the current value for "technology"
     * @throws DriverException if an error occured with the driver
     */
    public String getTechnology() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(BATTERY_PATH + "technology"));
            String line = reader.readLine();
            reader.close();
            return line;
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current value for "voltage_min_design"
     * 
     * @return the current value for "voltage_min_design"
     * @throws DriverException if an error occured with the driver
     */
    public float getVoltageMinDesign() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(BATTERY_PATH + "voltage_min_design"));
            String line = reader.readLine();
            reader.close();
            return (float) (Integer.parseInt(line) / 1000000.0);
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Get the current value for "voltage_min_design"
     * 
     * @return the current value for "voltage_min_design"
     * @throws DriverException if an error occured with the driver
     */
    public float getVoltageNow() throws DriverException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(BATTERY_PATH + "voltage_now"));
            String line = reader.readLine();
            reader.close();
            return (float) (Integer.parseInt(line) / 1000000.0);
        } catch (IOException e) {
            throw new DriverException("Driver error");
        }
    }

    /**
     * Calculate the wear level of the battery
     * 
     * @return the wear level of the battery (in %)
     * @throws DriverException if an error occured with the driver
     */
    public float getWearLevel() throws DriverException {
        float energyFull = this.getEnergyFull();
        float energyFullDesign = this.getEnergyFullDesign();
        return (energyFull / energyFullDesign * 100);
    }

    /**
     * Calculate the percentage of the battery
     * 
     * @return the percentage of the battery (in %)
     * @throws DriverException if an error occured with the driver
     */
    public float getPercentage() throws DriverException {
        float energyNow = this.getEnergyNow();
        float energyFull = this.getEnergyFull();
        return (energyNow / energyFull * 100);
    }

    /**
     * Calculate the current of the battery
     * 
     * @return the current of the battery (in A)
     * @throws DriverException if an error occured with the driver
     */
    public float getCurrent() throws DriverException {
        float voltage = this.getVoltageNow();
        float power = this.getPowerNow();
        return power / voltage;
    }

    /**
     * Get the current AC adapter connection state
     * 
     * @return true if the AC adapter is connected, false otherwise
     * @throws DriverException if an error occured with the driver
     */
    public boolean isConnected() throws DriverException {
        String status = this.getStatus();
        return !status.equals("Discharging");
    }

}