package model;

/**
 * DriverException class
 * Represents an exception thrown when an error occured with the driver
 * 
 * @author Guyaume MORICE
 */
public class DriverException extends Exception {

    /**
     * Constructor of the DriverException class
     * 
     * @param message Message of the exception
     */
    public DriverException(String message) {
        super(message);
    }

}
