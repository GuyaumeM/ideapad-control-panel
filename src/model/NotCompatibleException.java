package model;

/**
 * NotCompatibleException class
 * Represents an exception thrown when the computer is not compatible with this
 * program
 * 
 * @author Guyaume MORICE
 */
public class NotCompatibleException extends Exception {

    /**
     * Constructor of the NotCompatibleException class
     * 
     * @param message Message of the exception
     */
    public NotCompatibleException(String message) {
        super(message);
    }

}
