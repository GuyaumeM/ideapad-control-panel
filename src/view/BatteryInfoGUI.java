package view;

import javax.swing.*;
import java.awt.*;
import controller.*;

/**
 * BatteryInfoGUI class
 * View for the battery informations window
 * 
 * @author Guyaume MORICE
 */
public class BatteryInfoGUI extends JFrame {

    // GUI components
    private JLabel batteryManufacturerLabel;
    private JLabel batteryModelLabel;
    private JLabel batterySerialNumberLabel;
    private JLabel batteryChemistryLabel;
    private JLabel batteryDesignVoltageLabel;
    private JLabel batteryDesignCapacityLabel;
    private JLabel batteryCapacityLabel;
    private JLabel batteryStateOfChargeLabel;
    private JLabel batteryPercentageLabel;
    private JLabel batteryWearLabel;
    private JLabel batteryPowerLabel;
    private JLabel batteryVoltageLabel;
    private JLabel batteryCurrentLabel;
    private JLabel batteryStatusLabel;
    private JButton closeButton;

    // Constants
    private final Font INFO_FONT = new Font("Arial", Font.BOLD, 15);
    private final String ASSETS_PATH = System.getProperty("java.class.path") + "/../assets/";
    private final Color DISABLED_COLOR = Color.WHITE;
    private final Dimension BUTTON_DIMENSION = new Dimension(150, 50);

    /**
     * BatteryInfoGUI view constructor
     * 
     * @param controller Controller of the application
     * @param listener   Listener for the buttons of the view
     */
    public BatteryInfoGUI(BatteryInfoController listener) {

        // Panel initialization
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

        // Initialize the components
        this.batteryManufacturerLabel = new JLabel();
        this.batteryModelLabel = new JLabel();
        this.batterySerialNumberLabel = new JLabel();
        this.batteryChemistryLabel = new JLabel();
        this.batteryDesignVoltageLabel = new JLabel();
        this.batteryDesignCapacityLabel = new JLabel();
        this.batteryCapacityLabel = new JLabel();
        this.batteryStateOfChargeLabel = new JLabel();
        this.batteryPercentageLabel = new JLabel();
        this.batteryWearLabel = new JLabel();
        this.batteryPowerLabel = new JLabel();
        this.batteryVoltageLabel = new JLabel();
        this.batteryCurrentLabel = new JLabel();
        this.batteryStatusLabel = new JLabel();
        this.closeButton = new JButton("Close");

        this.batteryManufacturerLabel.setFont(INFO_FONT);
        this.batteryModelLabel.setFont(INFO_FONT);
        this.batterySerialNumberLabel.setFont(INFO_FONT);
        this.batteryChemistryLabel.setFont(INFO_FONT);
        this.batteryDesignVoltageLabel.setFont(INFO_FONT);
        this.batteryDesignCapacityLabel.setFont(INFO_FONT);
        this.batteryCapacityLabel.setFont(INFO_FONT);
        this.batteryStateOfChargeLabel.setFont(INFO_FONT);
        this.batteryPercentageLabel.setFont(INFO_FONT);
        this.batteryWearLabel.setFont(INFO_FONT);
        this.batteryPowerLabel.setFont(INFO_FONT);
        this.batteryVoltageLabel.setFont(INFO_FONT);
        this.batteryCurrentLabel.setFont(INFO_FONT);
        this.batteryStatusLabel.setFont(INFO_FONT);
        this.closeButton.setPreferredSize(BUTTON_DIMENSION);
        this.closeButton.setBackground(DISABLED_COLOR);

        // Initialize the panels
        JPanel batteryPanel = new JPanel();
        batteryPanel.setLayout(new GridLayout(15, 2));
        batteryPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        batteryPanel.add(new JLabel("Vendor : "));
        batteryPanel.add(this.batteryManufacturerLabel);
        batteryPanel.add(new JLabel("Model : "));
        batteryPanel.add(this.batteryModelLabel);
        batteryPanel.add(new JLabel("Serial Number : "));
        batteryPanel.add(this.batterySerialNumberLabel);
        batteryPanel.add(new JLabel("Technology : "));
        batteryPanel.add(this.batteryChemistryLabel);
        batteryPanel.add(new JLabel("Design Voltage : "));
        batteryPanel.add(this.batteryDesignVoltageLabel);
        batteryPanel.add(new JLabel("Design Capacity : "));
        batteryPanel.add(this.batteryDesignCapacityLabel);
        batteryPanel.add(new JLabel("Current Capacity : "));
        batteryPanel.add(this.batteryCapacityLabel);
        batteryPanel.add(new JLabel("State of Charge : "));
        batteryPanel.add(this.batteryStateOfChargeLabel);
        batteryPanel.add(new JLabel("Percentage : "));
        batteryPanel.add(this.batteryPercentageLabel);
        batteryPanel.add(new JLabel("Wear Level: "));
        batteryPanel.add(this.batteryWearLabel);
        batteryPanel.add(new JLabel("Power : "));
        batteryPanel.add(this.batteryPowerLabel);
        batteryPanel.add(new JLabel("Voltage : "));
        batteryPanel.add(this.batteryVoltageLabel);
        batteryPanel.add(new JLabel("Current : "));
        batteryPanel.add(this.batteryCurrentLabel);
        batteryPanel.add(new JLabel("Status : "));
        batteryPanel.add(this.batteryStatusLabel);

        JPanel closeButtonPanel = new JPanel();
        closeButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        closeButtonPanel.add(this.closeButton);

        // Add components to the main panel
        mainPanel.add(batteryPanel);
        mainPanel.add(closeButtonPanel);

        // Add a link between the button and the listener
        this.closeButton.addActionListener(listener);

        // Window configuration
        this.setTitle("Battery Informations");
        this.setFocusable(true);
        this.setResizable(false);
        this.setContentPane(mainPanel);
        this.setSize(new Dimension(350, 400));

        Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension windowDim = this.getSize();
        this.setLocation(screenDim.width / 2 - windowDim.width / 2, screenDim.height / 2 - windowDim.height / 2);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.addWindowListener(listener);
        this.setIconImage(new ImageIcon(ASSETS_PATH + "icon.png").getImage());

        this.setVisible(true);
        this.setAlwaysOnTop(true);
    }

    /**
     * Getter for the label batteryManufacturerLabel
     * 
     * @return batteryManufacturerLabel
     */
    public JLabel getBatteryManufacturerLabel() {
        return this.batteryManufacturerLabel;
    }

    /**
     * Getter for the label batteryModelLabel
     * 
     * @return batteryModelLabel
     */
    public JLabel getBatteryModelLabel() {
        return this.batteryModelLabel;
    }

    /**
     * Getter for the label batterySerialNumberLabel
     * 
     * @return batterySerialNumberLabel
     */
    public JLabel getBatterySerialNumberLabel() {
        return this.batterySerialNumberLabel;
    }

    /**
     * Getter for the label batteryChemistryLabel
     * 
     * @return batteryChemistryLabel
     */
    public JLabel getBatteryChemistryLabel() {
        return this.batteryChemistryLabel;
    }

    /**
     * Getter for the label batteryDesignVoltageLabel
     * 
     * @return batteryDesignVoltageLabel
     */
    public JLabel getBatteryDesignVoltageLabel() {
        return this.batteryDesignVoltageLabel;
    }

    /**
     * Getter for the label batteryDesignCapacityLabel
     * 
     * @return batteryDesignCapacityLabel
     */
    public JLabel getBatteryDesignCapacityLabel() {
        return this.batteryDesignCapacityLabel;
    }

    /**
     * Getter for the label batteryCapacityLabel
     * 
     * @return batteryCapacityLabel
     */
    public JLabel getBatteryCapacityLabel() {
        return this.batteryCapacityLabel;
    }

    /**
     * Getter for the label batteryStateOfChargeLabel
     * 
     * @return batteryStateOfChargeLabel
     */
    public JLabel getBatteryStateOfChargeLabel() {
        return this.batteryStateOfChargeLabel;
    }

    /**
     * Getter for the label batteryPercentageLabel
     * 
     * @return batteryPercentageLabel
     */
    public JLabel getBatteryPercentageLabel() {
        return this.batteryPercentageLabel;
    }

    /**
     * Getter for the label batteryWearLabel
     * 
     * @return batteryWearLabel
     */
    public JLabel getBatteryWearLabel() {
        return this.batteryWearLabel;
    }

    /**
     * Getter for the label batteryPowerLabel
     * 
     * @return batteryPowerLabel
     */
    public JLabel getBatteryPowerLabel() {
        return this.batteryPowerLabel;
    }

    /**
     * Getter for the label batteryVoltageLabel
     * 
     * @return batteryVoltageLabel
     */
    public JLabel getBatteryVoltageLabel() {
        return this.batteryVoltageLabel;
    }

    /**
     * Getter for the label batteryCurrentLabel
     * 
     * @return batteryCurrentLabel
     */
    public JLabel getBatteryCurrentLabel() {
        return this.batteryCurrentLabel;
    }

    /**
     * Getter for the label batteryStatusLabel
     * 
     * @return batteryStatusLabel
     */
    public JLabel getBatteryStatusLabel() {
        return this.batteryStatusLabel;
    }

    /**
     * Getter for the button closeButton
     * 
     * @return closeButton
     */
    public JButton getCloseButton() {
        return this.closeButton;
    }

}