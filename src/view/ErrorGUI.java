package view;

import javax.swing.*;
import java.awt.*;

/**
 * ErrorGUI class
 * Displays an error message in a window
 * 
 * @author Guyaume MORICE
 */
public class ErrorGUI extends JFrame {

    // Constants
    private final String ASSETS_PATH = System.getProperty("java.class.path") + "/../assets/";

    /**
     * ErrorGUI view constructor
     * 
     * @param e Exception to display
     */
    public ErrorGUI(Exception e) {

        // Panel & components initialization
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(1, 1));
        mainPanel.add(new JLabel(e.getMessage()));

        // Window configuration
        this.setTitle("Error");
        this.setFocusable(true);
        this.setContentPane(mainPanel);
        this.setSize(400, 100);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension windowDim = this.getSize();
        this.setLocation(screenDim.width / 2 - windowDim.width / 2, screenDim.height / 2 - windowDim.height / 2);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setIconImage(new ImageIcon(ASSETS_PATH + "icon.png").getImage());
        this.setVisible(true);
    }
}