package view;

import javax.swing.*;
import java.awt.*;

import controller.*;

/**
 * MainGUI view class
 * Main menu of the application
 * 
 * @author Guyaume MORICE
 */
public class MainGUI extends JFrame {

    // GUI components
    private JButton power1Button;
    private JButton power2Button;
    private JButton power3Button;
    private JButton powerFanButton;
    private JButton utilsFnButton;
    private JButton utilsUsbButton;
    private JButton utilsTouchpadButton;
    private JButton batteryConservationButton;
    private JTextField batteryPercentageField;
    private JButton batteryPercentageButton;
    private JLabel batteryPercentageLabel;
    private JLabel batteryWearLabel;
    private JLabel batteryPowerLabel;
    private JLabel batteryVoltageLabel;
    private JButton batteryInfoButton;

    // Constants
    private final String ASSETS_PATH = System.getProperty("java.class.path") + "/../assets/";
    private final Dimension BUTTON_DIMENSION = new Dimension(100, 100);
    private final Font TITLE_FONT = new Font("Arial", Font.BOLD, 20);
    private final Font FUNCTION_FONT = new Font("Arial", Font.BOLD, 10);
    private final Font INFO_FONT = new Font("Arial", Font.BOLD, 15);
    private final Font CREDITS_FONT = new Font("Arial", Font.PLAIN, 10);

    /**
     * MainGUI view constructor
     * 
     * @param controller Controller of the application
     * @param listener   Listener for the buttons of the view
     */
    public MainGUI(MainController listener) {

        // Main panels initialization
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));

        // Initialize the components
        this.power1Button = new JButton();
        this.power2Button = new JButton();
        this.power3Button = new JButton();
        this.powerFanButton = new JButton();
        this.utilsFnButton = new JButton();
        this.utilsUsbButton = new JButton();
        this.utilsTouchpadButton = new JButton();
        this.batteryConservationButton = new JButton();
        this.batteryPercentageField = new JTextField();
        this.batteryPercentageButton = new JButton("Set");
        this.batteryPercentageLabel = new JLabel();
        this.batteryWearLabel = new JLabel();
        this.batteryPowerLabel = new JLabel();
        this.batteryVoltageLabel = new JLabel();
        this.batteryInfoButton = new JButton("Show Battery Informations");

        this.power1Button.setPreferredSize(BUTTON_DIMENSION);
        this.power2Button.setPreferredSize(BUTTON_DIMENSION);
        this.power3Button.setPreferredSize(BUTTON_DIMENSION);
        this.powerFanButton.setPreferredSize(BUTTON_DIMENSION);
        this.utilsFnButton.setPreferredSize(BUTTON_DIMENSION);
        this.utilsUsbButton.setPreferredSize(BUTTON_DIMENSION);
        this.utilsTouchpadButton.setPreferredSize(BUTTON_DIMENSION);
        this.batteryConservationButton.setPreferredSize(BUTTON_DIMENSION);
        this.batteryPercentageField.setFont(INFO_FONT);
        this.batteryPercentageField.setPreferredSize(new Dimension(80, 30));
        this.batteryPercentageButton = new JButton("Set");
        this.batteryPercentageButton.setPreferredSize(new Dimension(100, 50));
        this.batteryPercentageLabel.setFont(INFO_FONT);
        this.batteryWearLabel.setFont(INFO_FONT);
        this.batteryPowerLabel.setFont(INFO_FONT);
        this.batteryVoltageLabel.setFont(INFO_FONT);
        this.batteryInfoButton.setPreferredSize(new Dimension(225, 70));

        // Power panel initialization
        JPanel powerTitlePanel = new JPanel();
        powerTitlePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 30));
        JLabel powerTitle = new JLabel("Power");
        powerTitle.setFont(TITLE_FONT);
        powerTitlePanel.add(powerTitle);

        JPanel power1PanelVertical = new JPanel();
        power1PanelVertical.setLayout(new BoxLayout(power1PanelVertical, BoxLayout.Y_AXIS));
        JLabel power1LabelTop = new JLabel("Battery Saving");
        JLabel power1LabelBottom = new JLabel(" ");
        power1LabelTop.setFont(FUNCTION_FONT);
        power1LabelBottom.setFont(FUNCTION_FONT);
        power1PanelVertical.add(this.power1Button);
        power1PanelVertical.add(power1LabelTop);
        power1PanelVertical.add(power1LabelBottom);

        JPanel power2PanelVertical = new JPanel();
        power2PanelVertical.setLayout(new BoxLayout(power2PanelVertical, BoxLayout.Y_AXIS));
        JLabel power2LabeTop = new JLabel("Intelligent");
        JLabel power2LabelBottom = new JLabel("Cooling");
        power2LabeTop.setFont(FUNCTION_FONT);
        power2LabelBottom.setFont(FUNCTION_FONT);
        power2PanelVertical.add(this.power2Button);
        power2PanelVertical.add(power2LabeTop);
        power2PanelVertical.add(power2LabelBottom);

        JPanel power3PanelVertical = new JPanel();
        power3PanelVertical.setLayout(new BoxLayout(power3PanelVertical, BoxLayout.Y_AXIS));
        JLabel power3LabeTop = new JLabel("Extreme");
        JLabel power3LabelBottom = new JLabel("Performance");
        power3LabeTop.setFont(FUNCTION_FONT);
        power3LabelBottom.setFont(FUNCTION_FONT);
        power3PanelVertical.add(this.power3Button);
        power3PanelVertical.add(power3LabeTop);
        power3PanelVertical.add(power3LabelBottom);

        JPanel powerModePanel = new JPanel();
        powerModePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        powerModePanel.add(power1PanelVertical);
        powerModePanel.add(power2PanelVertical);
        powerModePanel.add(power3PanelVertical);

        JPanel powerFanPanelVertical = new JPanel();
        powerFanPanelVertical.setLayout(new BoxLayout(powerFanPanelVertical, BoxLayout.Y_AXIS));
        JLabel powerFanLabel = new JLabel("Fan Turbo");
        powerFanLabel.setFont(FUNCTION_FONT);
        powerFanPanelVertical.add(this.powerFanButton);
        powerFanPanelVertical.add(powerFanLabel);

        JPanel powerFanPanel = new JPanel();
        powerFanPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        powerFanPanel.add(powerFanPanelVertical);

        JPanel powerPanel = new JPanel();
        powerPanel.setLayout(new BoxLayout(powerPanel, BoxLayout.Y_AXIS));
        powerPanel.add(powerTitlePanel);
        powerPanel.add(powerModePanel);
        powerPanel.add(powerFanPanel);
        powerPanel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));

        // Utils panel initialization
        JLabel utilsTitle = new JLabel("Utilities");
        utilsTitle.setFont(TITLE_FONT);
        JPanel utilsTitlePanel = new JPanel();
        utilsTitlePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 30));
        utilsTitlePanel.add(utilsTitle);

        JPanel utilsFnPanelVertical = new JPanel();
        utilsFnPanelVertical.setLayout(new BoxLayout(utilsFnPanelVertical, BoxLayout.Y_AXIS));
        JLabel utilsFnLabel = new JLabel("Function Keys");
        utilsFnLabel.setFont(FUNCTION_FONT);
        utilsFnPanelVertical.add(this.utilsFnButton);
        utilsFnPanelVertical.add(utilsFnLabel);

        JPanel utilsUsbPanelVertical = new JPanel();
        utilsUsbPanelVertical.setLayout(new BoxLayout(utilsUsbPanelVertical, BoxLayout.Y_AXIS));
        JLabel utilsUsbLabel = new JLabel("USB Charging");
        utilsUsbLabel.setFont(FUNCTION_FONT);
        utilsUsbPanelVertical.add(this.utilsUsbButton);
        utilsUsbPanelVertical.add(utilsUsbLabel);

        JPanel utilsTouchpadPanelVertical = new JPanel();
        utilsTouchpadPanelVertical.setLayout(new BoxLayout(utilsTouchpadPanelVertical, BoxLayout.Y_AXIS));
        JLabel utilsTouchpadLabel = new JLabel("Touchpad Lock");
        utilsTouchpadLabel.setFont(FUNCTION_FONT);
        utilsTouchpadPanelVertical.add(this.utilsTouchpadButton);
        utilsTouchpadPanelVertical.add(utilsTouchpadLabel);

        JPanel utilsButtonsPanel = new JPanel();
        utilsButtonsPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        utilsButtonsPanel.add(utilsFnPanelVertical);
        utilsButtonsPanel.add(utilsUsbPanelVertical);
        utilsButtonsPanel.add(utilsTouchpadPanelVertical);

        JPanel utilsPanel = new JPanel();
        utilsPanel.setLayout(new BoxLayout(utilsPanel, BoxLayout.Y_AXIS));
        utilsPanel.add(utilsTitlePanel);
        utilsPanel.add(utilsButtonsPanel);
        utilsPanel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));

        // Battery panel initialization
        JLabel batteryTitle = new JLabel("Battery");
        batteryTitle.setFont(new Font("Arial", Font.BOLD, 20));
        JPanel batteryTitlePanel = new JPanel();
        batteryTitlePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 30));
        batteryTitlePanel.add(batteryTitle);

        JPanel batteryConservationPanelVertical = new JPanel();
        batteryConservationPanelVertical.setLayout(new BoxLayout(batteryConservationPanelVertical, BoxLayout.Y_AXIS));
        JLabel batteryConservationLabelTop = new JLabel("Battery");
        JLabel batteryConservationLabelBottom = new JLabel("Conservation");
        batteryConservationLabelTop.setFont(FUNCTION_FONT);
        batteryConservationLabelBottom.setFont(FUNCTION_FONT);
        batteryConservationPanelVertical.add(this.batteryConservationButton);
        batteryConservationPanelVertical.add(batteryConservationLabelTop);
        batteryConservationPanelVertical.add(batteryConservationLabelBottom);

        JPanel batteryConservationPanel = new JPanel();
        batteryConservationPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        batteryConservationPanel.add(batteryConservationPanelVertical);

        JPanel batteryPercentagePanel = new JPanel();
        batteryPercentagePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        JLabel chargeToLabel = new JLabel("Charge to : ");
        chargeToLabel.setFont(INFO_FONT);
        batteryPercentagePanel.add(chargeToLabel);
        batteryPercentagePanel.add(this.batteryPercentageField);
        batteryPercentagePanel.add(this.batteryPercentageButton);

        JPanel batteryInfoPanelGrid = new JPanel();
        batteryInfoPanelGrid.setLayout(new GridLayout(4, 2));
        batteryInfoPanelGrid.add(new JLabel("Percentage : "));
        batteryInfoPanelGrid.add(this.batteryPercentageLabel);
        batteryInfoPanelGrid.add(new JLabel("Wear level : "));
        batteryInfoPanelGrid.add(this.batteryWearLabel);
        batteryInfoPanelGrid.add(new JLabel("Power : "));
        batteryInfoPanelGrid.add(this.batteryPowerLabel);
        batteryInfoPanelGrid.add(new JLabel("Voltage : "));
        batteryInfoPanelGrid.add(this.batteryVoltageLabel);
        batteryInfoPanelGrid.setBorder(BorderFactory.createTitledBorder("Charge Status"));

        JPanel batteryInfoPanel = new JPanel();
        batteryInfoPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        batteryInfoPanel.add(batteryInfoPanelGrid);

        JPanel batteryInfoButtonPanel = new JPanel();
        batteryInfoButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        batteryInfoButtonPanel.add(this.batteryInfoButton);

        JPanel batteryPanel = new JPanel();
        batteryPanel.setLayout(new BoxLayout(batteryPanel, BoxLayout.Y_AXIS));
        batteryPanel.add(batteryTitlePanel);
        batteryPanel.add(batteryConservationPanel);
        batteryPanel.add(batteryPercentagePanel);
        batteryPanel.add(batteryInfoPanel);
        batteryPanel.add(batteryInfoButtonPanel);
        batteryPanel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));

        // Adding credits
        JPanel creditsPanel = new JPanel();
        creditsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        creditsPanel.setPreferredSize(new Dimension(0, 12));
        JLabel creditsLabel = new JLabel("Made by Guyaume MORICE  -  2023");
        creditsLabel.setFont(CREDITS_FONT);
        creditsPanel.add(creditsLabel);
        creditsPanel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));

        // Add components to the main panels
        leftPanel.add(powerPanel);
        leftPanel.add(utilsPanel);
        rightPanel.add(batteryPanel);
        rightPanel.add(creditsPanel);
        mainPanel.add(leftPanel);
        mainPanel.add(rightPanel);

        // Add a link between the buttons and the listener
        this.power1Button.addActionListener(listener);
        this.power2Button.addActionListener(listener);
        this.power3Button.addActionListener(listener);
        this.powerFanButton.addActionListener(listener);
        this.utilsFnButton.addActionListener(listener);
        this.utilsUsbButton.addActionListener(listener);
        this.utilsTouchpadButton.addActionListener(listener);
        this.batteryConservationButton.addActionListener(listener);
        this.batteryPercentageButton.addActionListener(listener);
        this.batteryInfoButton.addActionListener(listener);

        // Window configuration
        this.setTitle("Lenovo Ideapad Control Panel");
        this.setFocusable(true);
        this.setResizable(false);
        this.setContentPane(mainPanel);
        this.pack();

        Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension windowDim = this.getSize();
        this.setLocation(screenDim.width / 2 - windowDim.width / 2, screenDim.height / 2 - windowDim.height / 2);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setIconImage(new ImageIcon(ASSETS_PATH + "icon.png").getImage());

        this.setVisible(true);
    }

    /**
     * Getter for the button power1Button
     * 
     * @return power1Button
     */
    public JButton getPower1Button() {
        return this.power1Button;
    }

    /**
     * Getter for the button power2Button
     * 
     * @return power2Button
     */
    public JButton getPower2Button() {
        return this.power2Button;
    }

    /**
     * Getter for the button power3Button
     * 
     * @return power3Button
     */
    public JButton getPower3Button() {
        return this.power3Button;
    }

    /**
     * Getter for the button powerFanButton
     * 
     * @return powerFanButton
     */
    public JButton getPowerFanButton() {
        return this.powerFanButton;
    }

    /**
     * Getter for the button utilsFnButton
     * 
     * @return utilsFnButton
     */
    public JButton getUtilsFnButton() {
        return this.utilsFnButton;
    }

    /**
     * Getter for the button utilsUsbButton
     * 
     * @return utilsUsbButton
     */
    public JButton getUtilsUsbButton() {
        return this.utilsUsbButton;
    }

    /**
     * Getter for the button utilsTouchpadButton
     * 
     * @return utilsTouchpadButton
     */
    public JButton getUtilsTouchpadButton() {
        return this.utilsTouchpadButton;
    }

    /**
     * Getter for the button batteryConservationButton
     * 
     * @return batteryConservationButton
     */
    public JButton getBatteryConservationButton() {
        return this.batteryConservationButton;
    }

    /**
     * Getter for the JTextField batteryPercentageField
     * 
     * @return batteryPercentageField
     */
    public JTextField getBatteryPercentageField() {
        return this.batteryPercentageField;
    }

    /**
     * Getter for the button batteryPercentageButton
     * 
     * @return batteryPercentageButton
     */
    public JButton getBatteryPercentageButton() {
        return this.batteryPercentageButton;
    }

    /**
     * Getter for the label batteryPercentageLabel
     * 
     * @return batteryPercentageLabel
     */
    public JLabel getBatteryPercentageLabel() {
        return this.batteryPercentageLabel;
    }

    /**
     * Getter for the label batteryWearLabel
     * 
     * @return batteryWearLabel
     */
    public JLabel getBatteryWearLabel() {
        return this.batteryWearLabel;
    }

    /**
     * Getter for the label batteryPowerLabel
     * 
     * @return batteryPowerLabel
     */
    public JLabel getBatteryPowerLabel() {
        return this.batteryPowerLabel;
    }

    /**
     * Getter for the label batteryVoltageLabel
     * 
     * @return batteryVoltageLabel
     */
    public JLabel getBatteryVoltageLabel() {
        return this.batteryVoltageLabel;
    }

    /**
     * Getter for the button batteryInfoButton
     * 
     * @return batteryInfoButton
     */
    public JButton getBatteryInfoButton() {
        return this.batteryInfoButton;
    }

}